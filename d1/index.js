/*
	CRUD OPERATIONS
*/

	/*
		Create Operation
			- add or insert operations add new documents to a collection. 

			Syntax:
			db.collection.insertOne(document) 
			db.collection.insertMany() 
	*/

		// Example: We will insert one document

		db.users.insertOne(
			{
				"firstName": "Jane",
				"lastName": "Doe",
				"age": 21,
				"contact":{
					"phone": "87654321",
					"emal": "janedoe@gmail.com"
				},
				"courses": ["CSS", "Javascript", "Python"],
				"department": "none"

			}
		)

		db.users.insertMany(
			[{
				"firstName": "Jane",
				"lastName": "Doe",
				"age": 21,
				"contact":{
					"phone": "87654321",
					"emal": "janedoe@gmail.com"
				},
				"courses": ["CSS", "Javascript", "Python"],
				"department": "none"

			},{
				"firstName": "Jane",
				"lastName": "Doe",
				"age": 21,
				"contact":{
					"phone": "87654321",
					"emal": "janedoe@gmail.com"
				},
				"courses": ["CSS", "Javascript", "Python"],
				"department": "none"

			}
				
			]
		)


		

	/*

		Read Operations

	
	*/


		db.users.find()


	/*

		Update Operations
			- modify existing documents in a collection

		syntax:

			db.collection.updateOne(filter, update) 
			db.collection.updateMany() 
			db.collection.replaceOne() 
		 
	*/
	

	db.user.insertOne({
		"firstName": "Test",
		"lastName": "Test",
		"age": 0,
		"contact":{
			"phone": "0",
			"emal": "test"
		},
		"courses": [],
		"department": "test"

	})

	db.user.updateOne(
		{"firstName": "Test"},
		{$set:{
			"firstName": "Bill",
			"lastName": "Gates",
			"age": 65,
			"contact": {
				"phone": "12345678",
				"email": "bill@gmail.com",
			},
			"courses": ["PHP", "Laravel", "HTML"],
			"department": "Operations",
			"status": "active"

			}

		}
	
	)


	//setting new field
	db.users.updateOne(
		{
			"_id": ObjectId("621f590e1c29176671333bc0")	

		},	
		{
			$set: {
				"isAdmin": true
			}
		}


		)


	//remove existing field
	db.users.updateOne(
		{
			"_id": ObjectId("621f590e1c29176671333bc0")	

		},	
		{
			$unset: {
				"isAdmin": true
			}
		}


		)


	// update multiple documents
		//update department to HR


		db.users.updateMany(
			{
				"department": "none"
			},
			{
				$set: {
					"department": "HR"
				}
			}
		)

		// update HR department with status active field

		db.users.updateMany(
			{
				"department": "HR"
			},
			{
				$set: {
					"status": "active"
				}
			}
		)

		//if using updateOne method, if you update HR Department documents isAdmin field to false what do you think will happened?

		db.users.updateOne(
			{
				"department": "HR"
			},
			{
				$set: {
					"isAdmin": false
				}
			}
		)
		// this will only update the first document that passes the filter document, not the entire documents




		// updateOne vs replaceOne

			




		db.users.insertOne({
					"firstName": "Test",
					"lastName": "Test",
					"age": 0,
					"contact": {
						"phone": "0",
						"email": "test"
					},
					"courses": [],
					"department": "test"
				})
		// using updateOne method, update the first name field to your first name
		db.users.updateOne({"firstName":"Test"},{$set:{"firstName":"Kevin"}})
			//update the fields specified in the update document parameter while keeping all the existing fields


		// using replaceOne method, update the last name field to your last name
		db.users.replaceOne({"lastName":"Test"},{"lastName":"Tud"})
		
			//replace the whole document with the replacement document parameter




//DELETE METHOD
	// - removes a single document from a collection.

	// deleteOne


	db.users.deleteOne(
	   {"_id": ObjectId("62200f62063b0958012d73b0")
	   }
	)


	//deleteMany

	db.users.insertMany(

		[
			{
				"courses":"JS",
				"price":1000
			},
			{
				"courses":"JAVA",
				"price":3000
			},
			{
				"courses":"JAVA",
				"price":4000
			}

		]

		)

	db.users.deleteMany(
		{"price": { $gte:3000}}

		)
	






